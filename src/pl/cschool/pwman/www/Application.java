package pl.cschool.pwman.www;

import java.util.List;
import java.util.Optional;
import java.util.Scanner;

public class Application {
    private Database database = new Database();
    private Scanner scan = new Scanner(System.in);
    private String enteredUsername;
    private String enteredDomain;


    public void runApplication() {
        boolean exit = false;
        int choice = 0;
        displayMenu();
        while (!exit) {
            System.out.println("Choose option (1-5): ");
            Scanner enterChoice = new Scanner(System.in);
            choice = enterChoice.nextInt();
            switch (choice) {
                case 1:
                    lookUpPassword();
                    break;
                case 2:
                    AddEntry();
                    break;
                case 3:
                    getNewPassword();
                    break;
                case 4:
                    database.generateReport();
                    break;
                case 5:
                    exit = true;
                    break;
                default:
                    System.out.println("Wrong option ");
                    break;
            }
        }
    }

    public void displayMenu() {

        System.out.println("P A S S W O R D   M A N A G E R" + "\n");
        System.out.println("1. Look up password");
        System.out.println("2. Add entry");
        System.out.println("3. Generate password");
        System.out.println("4. Generate report");
        System.out.println("5. Exit");

    }

    private void lookUpPassword() {
        promptForImput();
        Optional<String> password = database.getPassword(enteredUsername, enteredDomain);
        if (password.isPresent()) {
            System.out.println("Password for user " + enteredUsername + " in domain " + enteredDomain + ": " + password.get());
        } else {
            System.out.println("Entry does not exist");
        }
    }

    private void AddEntry() {
        String newPassword;
        promptForImput();
        DatabaseKey key = new DatabaseKey(enteredUsername, enteredDomain);
        if (database.checkIfKeyExists(key)) {
            System.out.println("Entry exists, enter new password: ");
            newPassword = scan.nextLine();
            database.changePassword(key, newPassword);
            System.out.println("Password has been changed");
        } else {
            System.out.println("Adding new entry, enter password: ");
            newPassword = scan.nextLine();
            database.addEntry(key, newPassword);
            System.out.println("New database entry has been added.");
        }
    }

    private void getNewPassword() {
        PasswordGenerator generator = new PasswordGenerator();
        System.out.println(generator.generatePassword());
    }

    private void promptForImput() {
        System.out.println("Enter username ");
        enteredUsername = scan.nextLine();
        System.out.println("Enter domain name: ");
        enteredDomain = scan.nextLine();
    }


    }


