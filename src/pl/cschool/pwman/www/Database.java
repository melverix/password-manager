package pl.cschool.pwman.www;

import java.util.*;

public class Database {
    private HashMap<DatabaseKey, String> dbEntries = new HashMap<DatabaseKey, String>();

    public Database() {
        DatabaseKey dbKey1 = new DatabaseKey("bob", "google");
        DatabaseKey dbKey2 = new DatabaseKey("zoe", "yahoo");
        DatabaseKey dbKey4 = new DatabaseKey("steve", "amazon");
        DatabaseKey dbKey3 = new DatabaseKey("ann", "amazon");

        dbEntries.put(dbKey1, "key1Pass");
        dbEntries.put(dbKey2, "key2Pass");
        dbEntries.put(dbKey3, "key3Pass");
        dbEntries.put(dbKey4, "key44Pass");
    }

    public Optional<String> getPassword(String username, String domain) {
        DatabaseKey key = new DatabaseKey(username, domain);
        return Optional.ofNullable(dbEntries.get(key));
    }

    public boolean checkIfKeyExists(DatabaseKey comparedKey) {
        return dbEntries.containsKey(comparedKey);
    }

    public void changePassword(DatabaseKey key, String newPassword) {
        dbEntries.replace(key, newPassword);
    }

    public void addEntry(DatabaseKey key, String password) {
        dbEntries.put(key, password);
    }

   public void generateReport (){
        List<DatabaseKey> keys = new ArrayList<>(dbEntries.keySet());
        Collections.sort(keys,new DatabaseKeyComparator());
        for (DatabaseKey key: keys){
            String domain = key.getDomain();
            String user = key.getUsername();
            String pass = getPassword(user, domain).get();
            System.out.println(domain + ", " + user + ", " + pass);
        }
    }
}
