package pl.cschool.pwman.www;

import java.util.Objects;
import java.util.StringJoiner;

public class DatabaseKey {

    private String username;
    private String domain;

    public DatabaseKey(String username, String domain) {
        this.username = username;
        this.domain = domain;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", DatabaseKey.class.getSimpleName() + "[", "]")
                .add("username='" + username + "'")
                .add("domain='" + domain + "'")
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DatabaseKey)) return false;
        DatabaseKey that = (DatabaseKey) o;
        return Objects.equals(getUsername(), that.getUsername()) &&
                Objects.equals(getDomain(), that.getDomain());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUsername(), getDomain());
    }
}