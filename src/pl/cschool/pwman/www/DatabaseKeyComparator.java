package pl.cschool.pwman.www;

import java.util.Comparator;

public class DatabaseKeyComparator implements Comparator<DatabaseKey> {
    @Override
    public int compare(DatabaseKey o1, DatabaseKey o2) {
        int result = o1.getDomain().compareTo(o2.getDomain());
        if (result == 0){
            result = o1.getUsername().compareTo(o2.getUsername());
        }
        return result;
    }
}
