package pl.cschool.pwman.www;

import java.security.SecureRandom;

public class PasswordGenerator {

    private static SecureRandom random = new SecureRandom();

    /**
     * different dictionaries used
     */
    private static final String ALPHA_CAPS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final String ALPHA = "abcdefghijklmnopqrstuvwxyz";
    private static final String NUMERIC = "0123456789";

    public static String generatePassword() {
        String result = "";
        for (int i = 1; i < 9; i++) {
            int set = random.nextInt(3);
            int index;
            switch(set){
                case 0:
                    index = random.nextInt(ALPHA.length());
                    result += ALPHA.charAt(index);
                    break;
                case 1:
                    index = random.nextInt(ALPHA_CAPS.length());
                    result += ALPHA_CAPS.charAt(index);
                    break;
                case 2:
                    index = random.nextInt(NUMERIC.length());
                    result += NUMERIC.charAt(index);
                    break;
            }
        }
        return result;
    }
}